#!/usr/bin/python3
from tkinter import *
from tkinter import filedialog as fd 
from PIL import ImageTk,Image
import time
import os
window = Tk()
window.title("KING-DUB")
w, h = window.winfo_screenwidth(), window.winfo_screenheight()
window.geometry("%dx%d+0+0" % (w, h))
background_image=PhotoImage(file="/home/jon/make_a_dub/.bg.png")
background_label = Label(image=background_image)
background_label.place(x=0, y=0, relwidth=1, relheight=1)

message = StringVar()
message.set(' Download')
downloading = Label(window,textvariable=message)
downloading.grid(column=2,row=3)

def dub():
	os.system("./mkdb '{}' {}".format(local_input.get(),dubName.get()))

def browse_File_System():
	victimVideo = fd.askopenfilename() 
	local_input.delete(0,END)
	local_input.insert(0,victimVideo)

def loading():
	message.set(' In progress..')

def ytdl():
	
	os.system("youtube-dl -o ~/make_a_dub/originals/{}.%\(ext\)s {}".format(dubName.get(),url_input.get()))
	message.set(' Completed!')

title = Label(window, text= "MAKE-A_DUB")
title.grid(column=0, row=0)
 
dubName_label = Label(window, text= "Name this project")
dubName_label.grid(column=0, row=2)
dubName = Entry(window,width=20)
dubName.grid(column=1, row=2)
dubName.focus()

download_video_button = Button(window, text = "Download from Youtube", command = ytdl)
download_video_button.grid(column=0,row=3)
url_input = Entry(window,width=20)
url_input.grid(column=1, row=3)

browse_files_button = Button(text='Select Video',command=browse_File_System)
browse_files_button.grid(column=0, row=4)
local_input = Entry(window,width=20)
local_input.grid(column=1, row=4)

start_button = Button(window, text = "Start dubbing!", command = dub)
start_button.grid(column=0, row=5)


window.mainloop()
